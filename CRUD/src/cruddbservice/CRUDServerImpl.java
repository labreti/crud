package cruddbservice;

import java.util.Hashtable;
import javax.jws.WebService;

@WebService(endpointInterface = "cruddbservice.CRUDServer")
public class CRUDServerImpl implements CRUDServer {
	
	Hashtable<String,String> directory = new Hashtable<String,String>();

	@Override
	public boolean create(String userId, String address) {
		if ( directory.containsKey(userId) ) { return false; }
		directory.put(userId, address);
		return true;
	}

	@Override
	public boolean delete(String userId) {
		if ( ! directory.containsKey(userId) ) { return false; }
		directory.remove(userId);
		return true;
	}

	@Override
	public String read(String userId) {
		String address = directory.get(userId);
		if ( address == null ) { return null; } 
		return address;
	}

	@Override
	public boolean update(String userId, String address) {
		if ( ! directory.containsKey(userId) ) { return false; }
		directory.put(userId,address);
		return true;
	}
}
