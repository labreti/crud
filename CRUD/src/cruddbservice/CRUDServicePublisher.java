package cruddbservice;

import javax.xml.ws.Endpoint;

public class CRUDServicePublisher {

	public static void main(String[ ] args) {
		// Viene associato il servizio ad una URL (verificare con un browser!)
		Endpoint.publish("http://127.0.0.1:9898/CRUD", new CRUDServerImpl());
		System.out.println("Servizio CRUD attivo alla porta 9898");
	}

}
