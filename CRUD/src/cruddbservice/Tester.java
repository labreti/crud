package cruddbservice;

import javax.xml.namespace.QName;
import javax.xml.ws.Service;
import java.net.URL;

public class Tester {
	public static void main(String[] args) throws Exception {
		URL url = new URL("http://localhost:9898/CRUD?wsdl");
		// Definisco il "Qualified name" del servizio (URI,nome)
		// URI è derivato dal nome del package, nome dalla classe che implementa l'interfaccia
		QName qname = new QName("http://cruddbservice/", "CRUDServerImplService");
		// Ottengo la visibilità del servizio
		Service service = Service.create(url, qname);
		// Estraggo il mio endpoint del servizio
		CRUDServer eif = service.getPort(CRUDServer.class);

		System.out.println(eif.create("pippo","192.168.0.12"));
		System.out.println(eif.read("pippo"));
	}

}
