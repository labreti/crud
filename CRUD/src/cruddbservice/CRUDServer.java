package cruddbservice;

import javax.jws.WebService;
import javax.jws.WebMethod;
import javax.jws.soap.SOAPBinding;
import javax.jws.soap.SOAPBinding.Style;

@WebService                     // Dichiara che si tratta dell'interfaccia di un servizio
@SOAPBinding(style = Style.RPC) // Specifica l'implementazione del servizio
public interface CRUDServer {
    @WebMethod boolean create(String userId, String address);
    @WebMethod boolean update(String userId, String address);
    @WebMethod String read(String userId);
    @WebMethod boolean delete(String userId);
}
